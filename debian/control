Source: node-postcss-convert-values
Section: javascript
Priority: optional
Testsuite: autopkgtest-pkg-nodejs
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders: Pirate Praveen <praveen@debian.org>
Build-Depends:
 debhelper-compat (= 12)
 , nodejs
 , node-babel7
 , pkg-js-tools (>= 0.9.35~)
 , node-postcss (>= 5.0.11)
 , node-postcss-value-parser (>= 3.1.2)
Standards-Version: 4.5.0
Homepage: https://github.com/ben-eb/postcss-convert-values
Vcs-Git: https://salsa.debian.org/js-team/node-postcss-convert-values.git
Vcs-Browser: https://salsa.debian.org/js-team/node-postcss-convert-values

Package: node-postcss-convert-values
Architecture: all
Depends:
 ${misc:Depends}
 , nodejs
 , node-postcss (>= 5.0.11)
 , node-postcss-value-parser (>= 3.1.2)
Description: Convert values with PostCSS (e.g. ms -> s)
 This plugin reduces CSS size by converting values to use different units where
 possible; for example, 500ms can be represented as .5s.
 .
 Note that this plugin only covers conversions for duration and absolute length
 values. For color conversions, use postcss-colormin.
 .
 Node.js is an event-based server-side JavaScript engine.
